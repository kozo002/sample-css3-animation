module SampleCssAnime
  class App < Sinatra::Base
    register Sinatra::Namespace

    get('/') { haml :index }

    namespace %r{/stylesheets|/javascripts} do
      get (%r{/(.+)\.(css|js)}) do
        command = params[:captures].last == 'css' ? :scss : :coffee
        send(command, :"#{ command }/#{ params[:captures].first }")
      end
    end
  end
end
