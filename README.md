# Sample css3 animation
---

- digg flavored flip animation box
- worked on css3 trasition support browser


# How to run
---

    $ git clone git@github.com:kozo002/sample-css3-animation.git
    $ cd sample-css3-animation
    $ bundle install --path vendor/bundle --without production
    $ bundle exec shotgun
    $ open http://localhost:9393
