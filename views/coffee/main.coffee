checkClasses = (target, mode, klasses) ->
  result = false
  $.each klasses, (i, klass) ->
    result = target.hasClass(klass)
    switch mode
      when 'has'
        return false unless result
      when 'not has'
        return false if result
  result

jQuery.fn.hasClasses = (klasses) ->
  checkClasses @, 'has', klasses

jQuery.fn.notHasClasses = (klasses) ->
  !checkClasses @, 'not has', klasses

class BoxFlip
  DURATION = 600
  ROTATED = 'rotated'
  ACTED = 'acted'

  constructor: (@container_name) ->
    @$container = null

  bindClickContainer: ->
    $(document).on 'click', @container_name, $.proxy(@clickContainer, @)

  clickContainer: (e) ->
    e.preventDefault()
    @$container = $ e.currentTarget
    if @$container.notHasClasses [ROTATED, ACTED]
      @triggerFlip 'on'
    else if @$container.hasClasses [ROTATED, ACTED]
      @triggerFlip 'off'

  triggerFlip: (mode) ->
    method = if mode == 'on' then 'addClass' else 'removeClass'
    @$container[method](ROTATED)
    timer_id = setTimeout =>
      clearTimeout timer_id
      @$container[method](ACTED)
      @$container = null
    , DURATION

jQuery ->
  new BoxFlip('.box-container').bindClickContainer()
